require('dotenv').config();

const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URL);

const { filesRouter } = require('./routers/files');
const { authRouter } = require('./routers/auth');
const { usersRouter } = require('./routers/users');
const { notesRouter } = require('./routers/notes');
const { logsRouter } = require('./routers/logs');

const Logger = require('./utils/logger');

app.use(express.json());
app.use(morgan('tiny'));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

// QUESTION: why we are using POST:/api/files to create single file?
app.use('/api/files', filesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);
app.use('/api/logs', logsRouter);
app.use('/', (req, res) => res.json({message: 'ok'}));

const start = async () => {
  try {
    if (!fs.existsSync('storage')) {
      fs.mkdirSync('storage');
    }

    app.listen(process.env.APP_PORT);
    Logger.log('Application initialized');
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use((error, request) => {
  console.error(error);
  request.status(500).send({ message: 'Server error' });
});
