const bcrypt = require('bcryptjs');

const Logger = require('./../../utils/logger');
const { User } = require('./../../models/Users.js');

const getMyUser = async (request, response) => {
  Logger.log('Get my user');
  
  const user = await User
    .findById(request.user.id)
    .select(['_id', 'name', 'username']);
  
  if (user !== undefined) {
    Logger.log(`User ${user.username} was returned`);
    
    return response
      .status(200)
      .json({
        user
      });
  } else {
    unexpectedErrorHandler(request, response);
  }
};

const deleteMyUser = async (request, response) => {
  Logger.log('Deleting my user');
  
  await User.findById(request.user.id)
    .deleteOne()
    .then(result => {
      if (result.deletedCount) {
        Logger.log('Deleting my user is successful');
        return response.status(200).json({ message: 'Success' })
      } else {
        Logger.log('Nothing to delete');
        return response.status(400).json({ message: 'Nothing to delete' })
      }
    })
    .catch(unexpectedErrorHandler(request, response));
};

const updateMyUser = async (request, response) => {
  Logger.log('Patching my user');
  
  const { username, name, oldPassword, newPassword } = request.body;
  const user = await User.findById(request.user.id);
  
  if (oldPassword && newPassword && await bcrypt.compare(oldPassword, user.password)) {
    Logger.log('Changing data and password for my user');
    
    await User.findByIdAndUpdate(request.user.id, {
      username,
      name,
      password: await bcrypt.hash(newPassword, 10)
    });
  } else {
    Logger.log('Changing only data for my user');
    
    await User.findByIdAndUpdate(request.user.id, { username, name });
  }
  
  Logger.log('Updating data for my user is successful');
  
  return response
    .status(200)
    .json({ message: 'Success' });
};

function unexpectedErrorHandler(request, response) {
  return (error) => {
    Logger.error(`Unexpected error! ${error}`);
    
    response
      .status(500)
      .send({ 'message': `Unexpected error! ${error}` });
  };
}

module.exports = {
  getMyUser,
  deleteMyUser,
  updateMyUser
};
