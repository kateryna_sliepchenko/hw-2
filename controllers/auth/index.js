const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Logger = require('./../../utils/logger');
const Validator = require('./../../utils/validator');
const { User } = require('./../../models/Users.js');

const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;

const registerUser = async (request, response) => {
  Logger.log('Registering user');
  
  Validator(request, response)
    .required('username', 'password');
  
  const { name, username, password } = request.body;
  
  const user = new User({
    name,
    username,
    password: await bcrypt.hash(password, 10)
  });
  
  user.save()
    .then(saved => {
      Logger.log(`User ${username} successfully created`);
      
      return response.json({
        message: 'Success',
        _id: saved._id,
        name: saved.name,
        username: saved.username
      });
    })
    .catch(unexpectedErrorHandler(request, response));
}

const loginUser = async (request, response) => {
  Logger.log('Login user');
  
  Validator(request, response)
    .required('username', 'password');
  
  const username = request.body.username;
  const user = await User.findOne({ username });
  
  if (user && await bcrypt.compare(String(request.body.password), String(user.password))) {
    const payload = { username: user.username, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, JWT_SECRET_KEY);
    
    Logger.log(`Authentication for ${username} successful`);
    
    return response.status(200).json({
      message: 'Success',
      jwt_token: jwtToken
    });
  }
  
  Logger.log(`Authentication for ${username} failed`);
  return response.
    status(400)
    .json({ 'message': 'Not authorized' });
}


function unexpectedErrorHandler(request, response) {
  return (error) => {
    Logger.error(`Unexpected error! ${error}`);
    
    response
      .status(500)
      .send({ 'message': `Unexpected error! ${error}` });
  };
}

module.exports = {
  registerUser,
  loginUser
};
