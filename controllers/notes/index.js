const Logger = require('./../../utils/logger');
const Validator = require('./../../utils/validator');
const { Note } = require('./../../models/Notes.js');

const getAllNotes = async (request, response) => {
  Logger.log('Getting all notes');
  
  const { id: userId, username } = request.user;
  const { offset = 0, limit = 10 } = request.body;
  
  const query = Note
    .find({
      userId
    });
  
  const count = await query
    .clone()
    .count();
  
  const notes = await query
    .skip(offset)
    .limit(limit)
    .select(['_id', 'userId', 'completed', 'text', 'createdDate', 'updatedDate']);
  
  Logger.log(`For ${username} found ${count} notes, sent ${limit} notes with ${offset} offset.`);
  
  return response
    .status(200)
    .json({
      offset,
      limit,
      count,
      notes
    });
};

const createNote = (request, response) => {
  Logger.log('Creating a note');
  
  Validator(request, response)
    .required('text');
  
  const { text } = request.body;
  const { id: userId, username } = request.user;
  
  const note = new Note({
    userId,
    completed: false,
    text,
    createdDate: Date.now(),
    updatedDate: Date.now()
  });
  
  note.save()
    .then(saved => {
      Logger.log(`Note from ${username} successfully created`);
      
      return response.json({
        message: 'Success',
        _id: saved._id,
        userId: saved.userId,
        completed: saved.completed,
        text: saved.text,
        createdDate: saved.createdDate,
        updatedDate: saved.updatedDate
      });
    })
    .catch(unexpectedErrorHandler(request, response));
};

const getNote = async (request, response) => {
  Validator(request, response)
    .required('id');
  
  const { id: noteId } = request.params;
  
  Logger.log(`Getting a note with id: ${noteId}`);
  
  const note = await Note
    .findById(noteId)
    .select(['_id', 'userId', 'completed', 'text', 'createdDate', 'updatedDate']);
  
  Logger.log(`Getting a note with id: ${noteId} is successful`);
  
  return response
    .status(200)
    .json({
      note
    });
};

// PUT: /api/notes
const updateNote = async (request, response) => {
  Validator(request, response)
    .required('id', 'text');
  
  const { id: noteId } = request.params;
  const { text } = request.body;
  Logger.log(`Updating a note with id: ${noteId}`);
  
  await Note.findByIdAndUpdate(noteId, {
      text,
      updatedDate: Date.now()
    })
    .then(() => {
      return response
        .status(200)
        .json({ message: 'Success' });
    })
    .catch(unexpectedErrorHandler(request, response));
};

const checkNote = async (request, response) => {
  Validator(request, response)
    .required('id');
  
  const { id: noteId } = request.params;
  const { completed } = request.body;
  Logger.log(`Checking a note with id: ${noteId}`);
  
  const query = Note
    .findById(noteId);
  
  const note = await query
    .clone();
  
  await query
    .updateOne({
      completed: completed !== undefined ? completed : !note.completed
    })
    .then(() => {
      Logger.log(`Checking a note with id: ${noteId} is successful`);
      
      return response
        .status(200)
        .json({ message: 'Success' });
    })
    .catch(unexpectedErrorHandler(request, response));
};

const deleteNote = async (request, response) => {
  Validator(request, response)
    .required('id');
  
  const { id: noteId } = request.params;
  
  Logger.log(`Deleting a note with id: ${noteId}`);
  
  await Note
    .findById(noteId)
    .deleteOne()
    .then(result => {
      if (result.deletedCount) {
        Logger.log('Deleting my user is successful');
        return response.status(200).json({ message: 'Success' })
      } else {
        Logger.log('Nothing to delete');
        return response.status(400).json({ message: 'Nothing to delete' })
      }
    })
    .catch(unexpectedErrorHandler(request, response));
};

function unexpectedErrorHandler(request, response) {
  return (error) => {
    Logger.error(`Unexpected error! ${error}`);
    
    response
      .status(500)
      .send({ 'message': `Unexpected error! ${error}` });
  };
}

module.exports = {
  getAllNotes,
  createNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote
};