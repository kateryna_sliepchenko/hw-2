const express = require('express');

const index = express.Router();

const { getMyUser, deleteMyUser, updateMyUser } = require('./../../controllers/users');
const { authMiddleware } = require('./../../middleware/auth');

index.get('/me', authMiddleware, getMyUser);
index.delete('/me', authMiddleware, deleteMyUser);
index.patch('/me', authMiddleware, updateMyUser);

module.exports = {
  usersRouter: index
};
