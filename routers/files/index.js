const express = require('express');
const index = express.Router();

const { createFile, getFiles, getFile, editFile, deleteFile } = require('./../../controllers/files');

index.post('/', createFile);
index.get('/', getFiles);
index.get('/:filename', getFile);
index.put('/:filename', editFile);
index.delete('/:filename', deleteFile);

module.exports = {
  filesRouter: index
};
