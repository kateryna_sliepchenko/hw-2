const express = require('express');

const index = express.Router();

const { registerUser, loginUser } = require('./../../controllers/auth');

index.post('/register', registerUser);
index.post('/login', loginUser);

module.exports = {
  authRouter: index
};
