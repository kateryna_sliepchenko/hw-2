const express = require('express');

const index = express.Router();

const { getAllNotes, createNote, getNote, updateNote, checkNote, deleteNote } = require('./../../controllers/notes');
const { authMiddleware } = require('./../../middleware/auth');
const { notesMiddleware } = require('./../../middleware/notes');

index.get('/', authMiddleware, getAllNotes);

index.post('/', authMiddleware, createNote);
index.get('/:id', [authMiddleware, notesMiddleware], getNote);
index.put('/:id', [authMiddleware, notesMiddleware], updateNote);
index.patch('/:id', [authMiddleware, notesMiddleware], checkNote);
index.delete('/:id', [authMiddleware, notesMiddleware], deleteNote);

module.exports = {
  notesRouter: index
};
