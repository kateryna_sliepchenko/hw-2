const { Note } = require('./../../models/Notes.js');
const Logger = require('./../../utils/logger');

const index = async (request, response, next) => {
  const { id: noteId } = request.params;
  const { id: userId } = request.user;
  
  if (!noteId) {
    return response
      .status(400)
      .json({ 'message': 'Please, provide note id param' });
  }
  
  const note = await Note.findById(noteId);
  
  if (!note) {
    Logger.log(`Note with id:${noteId} does not exist`);
    
    return response
      .status(400)
      .json({ 'message': `Note with id:${noteId} does not exist` });
  }
  
  if (note.userId === userId) {
    next();
  } else {
    Logger.log(`User:${userId} is trying to access to user's:${note.userId} note`);
    return response
      .status(400)
      .json({ 'message': 'You are not authorized for this note' });
  }
}

module.exports = {
  notesMiddleware: index
}
