const jwt = require('jsonwebtoken');

const index = (request, response, next) => {
    const { authorization } = request.headers;
    
    if (!authorization) {
        return response
            .status(401)
            .json({ 'message': 'Please, provide authorization header' });
    }
    
    const [, token] = authorization.split(' ');
    
    if (!token) {
        return response.status(401).json({ 'message': 'Please, include token to request' });
    }
    
    try {
        const tokenPayload = jwt.verify(token, process.env.JWT_SECRET_KEY);
        
        request.user = {
            id: tokenPayload.userId,
            username: tokenPayload.username,
            name: tokenPayload?.name
        }
        
        next();
    } catch (err) {
        return response
            .status(401)
            .json({ message: err.message });
    }
    
}

module.exports = {
    authMiddleware: index
}
